const net = require("net");

const clients = [];

// Crear el servidor TCP
const server = net.createServer((connection) => {
    clients.push(connection);
  // Enviar un mensaje al cliente cuando se establezca la conexión
  connection.write("Servidor iniciado correctamente!");
  console.log("Recibí una conexión remota");

  // Manejar los datos recibidos del cliente
  connection.on("data", (data) => {
    clients.forEach((client) => {
        client.write(data);
    })
  });
});

// Configurar el servidor para que escuche en el puerto 5000 y en la dirección localhost
server.listen(5000, "127.0.0.1");

