const net = require("net");
const readline = require("readline");

const client = new net.Socket();

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });

// Conectamos el cliente; este lleva tres parámetros: puerto, dirección, función flecha sobre qué hacer para cuando se conecte
client.connect(5000, "127.0.0.1", () => {
  console.log("Conexión exitosa");
  readUserMessage();
});

// Con "on" ponemos acciones para ciertos momentos, como triggers

client.on("close", () => {
  console.log("Conexión terminada");
});

client.on("data", (data) => {
  console.log(`${data}`);
});

function readUserMessage() {
    rl.question('Escribe un mensaje: ', (message) => {
      client.write(`Cliente 1: ${message}`);
      readUserMessage();
    });
  }

