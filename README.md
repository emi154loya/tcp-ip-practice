# TCP-IP-practice

## Clone

Corre el comando git clone git@gitlab.com:emi154loya/tcp-ip-practice.git

## Primer servidor TCP

Este proyecto tiene como objetivo realizar una practica funcional del comportamiento de un servidor TCP, teniendo un servidor y dos clientes conectados a este, por emdio del cual se puede comunicar.

La practica consiste en conectar 2 o mas clientes a un servidor y hacer un "chat general" de manera que los mensajes escritos por cada cliente sean visibles por los demás clientes conectados al servidor.

Este repositorio tiene 3 archivos:
- server.js
- client.js
- client2.js


## Como usarlo

Abre una terminal y colocate en la carpeta raiz del repositorio.

Despues corre el comando "node server.js"

Abre una nueva terminal localizada donde mismo y corre el comando "node client.js" 

Abre una nueva terminal localizada donde mismo y corre el comando "node client2.js"

Escribe en cualquiera de los 2 clientes para ver reflejado tu mensaje en las teminales de todos los clientes

## Autor

- Emiliano Loya Flores 357611
